using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Bewegliche_Feiertage_GUI
{
    public partial class Bewegliche_Feiertage : Form
    {
        private int faktor, day, mounth, year;
        private string helpday, helpmounth;

        public Bewegliche_Feiertage()
        {
            InitializeComponent();

            ToolTip help = new ToolTip();
            help.SetToolTip(txt_year, "Jahr von 1 bis 9999 eingeben");
            help.SetToolTip(txt_output, "Ausgabe");
            help.SetToolTip(cmd_ok, "Datum anzeigen");
            help.SetToolTip(cmd_clear, "Ein- und Ausgaben löschen");
            help.SetToolTip(cmd_end, "Programm beenden");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_year.Text = txt_output.Text = null;
            rbtn_karfreitag.Checked = rbtn_ostersonntag.Checked = rbtn_ostermontag.Checked =
                rbtn_christi_himmelfahrt.Checked = rbtn_pfingstsonntag.Checked = rbtn_pfingstmontag.Checked =
                rbtn_fronleichnam.Checked = false;
        }

        private void Berechnung()
        {
            try
            {
                int a, b, c, d, e, f;
                year = Convert.ToInt32(txt_year.Text);
                a = year % 19;
                b = year / 100;
                c = (8 * b + 13) / 25 - 2;
                d = b - (year / 400) - 2;
                e = (19 * a + ((15 - c + d) % 30)) % 30;
                if (e == 28 && a > 10)
                    e = 27;
                if (e == 29)
                    e = 28;
                f = (d + 6 * e + 2 * (year % 4) + 4 * (year % 7) + 6) % 7;
                day = e + f + 22;
                mounth = 3;
                day = day + faktor;
                if (day > 92)
                {
                    day -= 92;
                    mounth = 6;
                }
                if (day > 61)
                {
                    day -= 61;
                    mounth = 5;
                }
                if (day > 31)
                {
                    day -= 31;
                    mounth = 4;
                }
            }
            catch
            {
                MessageBox.Show("Bitte nur gültige vierstellige Jahre eingeben!");
                txt_output.Focus();
            }
        }

        private void cmd_ok_Click(object sender, EventArgs e)
        {
            if (rbtn_karfreitag.Checked)
            {
                faktor = -2;
                Berechnung();
                if (day < 10)
                    helpday = "0" + day;
                else
                    helpday = day.ToString();
                if (mounth < 10)
                    helpmounth = "0" + mounth;
                else
                    helpmounth = mounth.ToString();
                txt_output.Text = "Karfreitag: " + helpday + "." + helpmounth + "." + year;
            }

            if (rbtn_ostersonntag.Checked)
            {
                faktor = 0;
                Berechnung();
                if (day < 10)
                    helpday = "0" + day;
                else
                    helpday = day.ToString();
                if (mounth < 10)
                    helpmounth = "0" + mounth;
                else
                    helpmounth = mounth.ToString();
                txt_output.Text = "Ostersonntag: " + helpday + "." + helpmounth + "." + year;
            }

            if (rbtn_ostermontag.Checked)
            {
                faktor = 1;
                Berechnung();
                if (day < 10)
                    helpday = "0" + day;
                else
                    helpday = day.ToString();
                if (mounth < 10)
                    helpmounth = "0" + mounth;
                else
                    helpmounth = mounth.ToString();
                txt_output.Text = "Ostermontag: " + helpday + "." + helpmounth + "." + year;
            }

            if (rbtn_christi_himmelfahrt.Checked)
            {
                faktor = 39;
                Berechnung();
                if (day < 10)
                    helpday = "0" + day;
                else
                    helpday = day.ToString();
                if (mounth < 10)
                    helpmounth = "0" + mounth;
                else
                    helpmounth = mounth.ToString();
                txt_output.Text = "Christi Himmelfahrt: " + helpday + "." + helpmounth + "." + year;
            }

            if (rbtn_pfingstsonntag.Checked)
            {
                faktor = 49;
                Berechnung();
                if (day < 10)
                    helpday = "0" + day;
                else
                    helpday = day.ToString();
                if (mounth < 10)
                    helpmounth = "0" + mounth;
                else
                    helpmounth = mounth.ToString();
                txt_output.Text = "Pfingstsonntag: " + helpday + "." + helpmounth + "." + year;
            }

            if (rbtn_pfingstmontag.Checked)
            {
                faktor = 50;
                Berechnung();
                if (day < 10)
                    helpday = "0" + day;
                else
                    helpday = day.ToString();
                if (mounth < 10)
                    helpmounth = "0" + mounth;
                else
                    helpmounth = mounth.ToString();
                txt_output.Text = "Pfingstmontag: " + helpday + "." + helpmounth + "." + year;
            }

            if (rbtn_fronleichnam.Checked)
            {
                faktor = 60;
                Berechnung();
                if (day < 10)
                    helpday = "0" + day;
                else
                    helpday = day.ToString();
                if (mounth < 10)
                    helpmounth = "0" + mounth;
                else
                    helpmounth = mounth.ToString();
                txt_output.Text = "Fronleichnam: " + helpday + "." + helpmounth + "." + year;
            }
        }
    }
}
