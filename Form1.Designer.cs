namespace Bewegliche_Feiertage_GUI
{
    partial class Bewegliche_Feiertage
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txt_year = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_output = new System.Windows.Forms.TextBox();
            this.rbtn_karfreitag = new System.Windows.Forms.RadioButton();
            this.rbtn_ostersonntag = new System.Windows.Forms.RadioButton();
            this.rbtn_ostermontag = new System.Windows.Forms.RadioButton();
            this.rbtn_christi_himmelfahrt = new System.Windows.Forms.RadioButton();
            this.rbtn_pfingstsonntag = new System.Windows.Forms.RadioButton();
            this.rbtn_pfingstmontag = new System.Windows.Forms.RadioButton();
            this.rbtn_fronleichnam = new System.Windows.Forms.RadioButton();
            this.cmd_ok = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jahr";
            // 
            // txt_year
            // 
            this.txt_year.Location = new System.Drawing.Point(90, 75);
            this.txt_year.Name = "txt_year";
            this.txt_year.Size = new System.Drawing.Size(63, 22);
            this.txt_year.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(135, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(214, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Bewegliche Feiertage";
            // 
            // txt_output
            // 
            this.txt_output.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txt_output.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_output.Location = new System.Drawing.Point(204, 75);
            this.txt_output.Name = "txt_output";
            this.txt_output.ReadOnly = true;
            this.txt_output.Size = new System.Drawing.Size(257, 29);
            this.txt_output.TabIndex = 3;
            // 
            // rbtn_karfreitag
            // 
            this.rbtn_karfreitag.AutoSize = true;
            this.rbtn_karfreitag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_karfreitag.Location = new System.Drawing.Point(37, 127);
            this.rbtn_karfreitag.Name = "rbtn_karfreitag";
            this.rbtn_karfreitag.Size = new System.Drawing.Size(93, 20);
            this.rbtn_karfreitag.TabIndex = 4;
            this.rbtn_karfreitag.TabStop = true;
            this.rbtn_karfreitag.Text = "Karfreitag";
            this.rbtn_karfreitag.UseVisualStyleBackColor = true;
            // 
            // rbtn_ostersonntag
            // 
            this.rbtn_ostersonntag.AutoSize = true;
            this.rbtn_ostersonntag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_ostersonntag.Location = new System.Drawing.Point(37, 153);
            this.rbtn_ostersonntag.Name = "rbtn_ostersonntag";
            this.rbtn_ostersonntag.Size = new System.Drawing.Size(118, 20);
            this.rbtn_ostersonntag.TabIndex = 5;
            this.rbtn_ostersonntag.TabStop = true;
            this.rbtn_ostersonntag.Text = "Ostersonntag";
            this.rbtn_ostersonntag.UseVisualStyleBackColor = true;
            // 
            // rbtn_ostermontag
            // 
            this.rbtn_ostermontag.AutoSize = true;
            this.rbtn_ostermontag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_ostermontag.Location = new System.Drawing.Point(37, 180);
            this.rbtn_ostermontag.Name = "rbtn_ostermontag";
            this.rbtn_ostermontag.Size = new System.Drawing.Size(114, 20);
            this.rbtn_ostermontag.TabIndex = 6;
            this.rbtn_ostermontag.TabStop = true;
            this.rbtn_ostermontag.Text = "Ostermontag";
            this.rbtn_ostermontag.UseVisualStyleBackColor = true;
            // 
            // rbtn_christi_himmelfahrt
            // 
            this.rbtn_christi_himmelfahrt.AutoSize = true;
            this.rbtn_christi_himmelfahrt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_christi_himmelfahrt.Location = new System.Drawing.Point(37, 207);
            this.rbtn_christi_himmelfahrt.Name = "rbtn_christi_himmelfahrt";
            this.rbtn_christi_himmelfahrt.Size = new System.Drawing.Size(155, 20);
            this.rbtn_christi_himmelfahrt.TabIndex = 7;
            this.rbtn_christi_himmelfahrt.TabStop = true;
            this.rbtn_christi_himmelfahrt.Text = "Christi Himmelfahrt";
            this.rbtn_christi_himmelfahrt.UseVisualStyleBackColor = true;
            // 
            // rbtn_pfingstsonntag
            // 
            this.rbtn_pfingstsonntag.AutoSize = true;
            this.rbtn_pfingstsonntag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_pfingstsonntag.Location = new System.Drawing.Point(37, 234);
            this.rbtn_pfingstsonntag.Name = "rbtn_pfingstsonntag";
            this.rbtn_pfingstsonntag.Size = new System.Drawing.Size(128, 20);
            this.rbtn_pfingstsonntag.TabIndex = 8;
            this.rbtn_pfingstsonntag.TabStop = true;
            this.rbtn_pfingstsonntag.Text = "Pfingstsonntag";
            this.rbtn_pfingstsonntag.UseVisualStyleBackColor = true;
            // 
            // rbtn_pfingstmontag
            // 
            this.rbtn_pfingstmontag.AutoSize = true;
            this.rbtn_pfingstmontag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_pfingstmontag.Location = new System.Drawing.Point(37, 261);
            this.rbtn_pfingstmontag.Name = "rbtn_pfingstmontag";
            this.rbtn_pfingstmontag.Size = new System.Drawing.Size(124, 20);
            this.rbtn_pfingstmontag.TabIndex = 9;
            this.rbtn_pfingstmontag.TabStop = true;
            this.rbtn_pfingstmontag.Text = "Pfingstmontag";
            this.rbtn_pfingstmontag.UseVisualStyleBackColor = true;
            // 
            // rbtn_fronleichnam
            // 
            this.rbtn_fronleichnam.AutoSize = true;
            this.rbtn_fronleichnam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_fronleichnam.Location = new System.Drawing.Point(37, 288);
            this.rbtn_fronleichnam.Name = "rbtn_fronleichnam";
            this.rbtn_fronleichnam.Size = new System.Drawing.Size(119, 20);
            this.rbtn_fronleichnam.TabIndex = 10;
            this.rbtn_fronleichnam.TabStop = true;
            this.rbtn_fronleichnam.Text = "Fronleichnam";
            this.rbtn_fronleichnam.UseVisualStyleBackColor = true;
            // 
            // cmd_ok
            // 
            this.cmd_ok.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.cmd_ok.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_ok.Location = new System.Drawing.Point(37, 343);
            this.cmd_ok.Name = "cmd_ok";
            this.cmd_ok.Size = new System.Drawing.Size(79, 35);
            this.cmd_ok.TabIndex = 11;
            this.cmd_ok.Text = "&OK";
            this.cmd_ok.UseVisualStyleBackColor = false;
            this.cmd_ok.Click += new System.EventHandler(this.cmd_ok_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.BackColor = System.Drawing.Color.Blue;
            this.cmd_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_clear.ForeColor = System.Drawing.Color.White;
            this.cmd_clear.Location = new System.Drawing.Point(180, 343);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(98, 35);
            this.cmd_clear.TabIndex = 12;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = false;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.BackColor = System.Drawing.Color.Red;
            this.cmd_end.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_end.Location = new System.Drawing.Point(347, 343);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(89, 35);
            this.cmd_end.TabIndex = 13;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = false;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // Bewegliche_Feiertage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Aqua;
            this.ClientSize = new System.Drawing.Size(516, 404);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_ok);
            this.Controls.Add(this.rbtn_fronleichnam);
            this.Controls.Add(this.rbtn_pfingstmontag);
            this.Controls.Add(this.rbtn_pfingstsonntag);
            this.Controls.Add(this.rbtn_christi_himmelfahrt);
            this.Controls.Add(this.rbtn_ostermontag);
            this.Controls.Add(this.rbtn_ostersonntag);
            this.Controls.Add(this.rbtn_karfreitag);
            this.Controls.Add(this.txt_output);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_year);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Bewegliche_Feiertage";
            this.Text = "Bewegliche Feiertage";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_year;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_output;
        private System.Windows.Forms.RadioButton rbtn_karfreitag;
        private System.Windows.Forms.RadioButton rbtn_ostersonntag;
        private System.Windows.Forms.RadioButton rbtn_ostermontag;
        private System.Windows.Forms.RadioButton rbtn_christi_himmelfahrt;
        private System.Windows.Forms.RadioButton rbtn_pfingstsonntag;
        private System.Windows.Forms.RadioButton rbtn_pfingstmontag;
        private System.Windows.Forms.RadioButton rbtn_fronleichnam;
        private System.Windows.Forms.Button cmd_ok;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
    }
}

